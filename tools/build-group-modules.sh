#!/bin/bash

# Description:
# Script to build a group of modules
# Author: Wayne Lewis
# Date: 2020-07-13

# Options:
# -f: input inventory file
# -d: development build
# -e: abort on compile error
# -g: group to build
# -v: verbose
# -h: help

script=$(realpath "$0")
script_path=$(dirname "$script")

help() {
  echo "Build all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./build-group-modules.sh -f <path/to/inventory/file> -g <group_name> [-c module] [-d] [-e] [-v] [-o] [-s] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-g: specify group to build"
  echo "-d: build development version"
  echo "-e: abort on compile error"
  echo "-i: install the built module"
  echo "-v: be verbose"
  echo "-o: fix group ownership"
  echo "-s: skip if already exists"
  echo "-h: print this usage information"
}

# Process options
while getopts "f:g:deivosh" opt; do
  case $opt in
    f)
      input_file="$OPTARG"
      ;;
    g)
      group_name="$OPTARG"
      ;;
    d)
      development=true
      ;;
    e)
      abort_on_error=true
      ;;
    i)
      install=true
      ;;
    v)
      verbose=true
      ;;
    o)
      fix_group_ownership=true
      ;;
    s)
      skip_existing=true
      ;;
    h)
      help
      exit 0
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      help
      exit 1
      ;;
  esac
done

echo "Building group: $group_name"
echo "Using input file: $input_file"

fix_ownership() {
  install_path=$(make vars | grep "E3_MODULES_INSTALL_LOCATION = " | awk '{print $3}')
  install_parent=${install_path%/*}

  install_user=$(whoami)

  [[ "$1" = true ]] && verbose_flag=-v

  find "$install_parent" -user "$install_user" -exec chgrp "$verbose_flag" e3rw_group {} \;
  find "$install_parent" -user "$install_user" -type d -exec chmod "$verbose_flag" g+w {} \;
}

build_modules() {
  pwd=$(pwd)
  [[ "$verbose" = true ]] && echo "$pwd"
  [[ "$development" = true ]] && dev_prefix=dev

  # Iterate through the inventory file. # lines are ignored as comments.
  if [ -n "$group_name" ]; then
    for dir in $1; do
      [[ "$verbose" = true ]] && echo "Directory = $dir"
      [[ "$verbose" = true ]] && echo "Group = $group_name"
      if [ -d "$dir" ]; then
        pushd "$dir" || return

        if [[ "$skip_existing" = true && -d $(make vars | grep "E3_MODULES_INSTALL_LOCATION = " | awk '{print $3}') ]]; then
          echo "Skipping $dir"
          popd || return
          continue
        fi

        # Do a complete build of the modules
        make "$dev_prefix"clean
        make "$dev_prefix"init
        make "$dev_prefix"patch

        # The following two steps may abort
        make "$dev_prefix"build || { [[ "$abort_on_error" = true ]] && echo ">>>> Build failed on module $dir" >&2 && return 1; }
        if [[ "$install" = true ]]; then
          make "$dev_prefix"install SUDO=
        else
          [[ "$abort_on_error" = true ]] && echo ">>>> Install failed on module $dir" >&2 && return 1
        fi

        [[ "$fix_group_ownership" = true ]] && fix_ownership "$verbose"
        popd || return
      else
        echo "Could not find $dir" >&2
      fi
    done
  fi
}

[ ! -f "$input_file" ] && echo "Input file '$input_file' does not exist." >&2 && exit 1

[ -z "$group_name" ] && echo "You must specify a group" >&2 && exit 1

modules=$(python3 "$script_path"/get_inventory.py -g "$group_name" -o "$input_file")

if [ -z "$modules" ]; then
  echo "Error: no modules found for group $group_name in input file $input_file" >&2
  exit 1
fi

build_modules "$modules"
