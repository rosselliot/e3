#!/bin/bash

# Description:
# Script to update the EPICS base and require versions in E3 wrappers
# Author: Wayne Lewis
# Date: 2020-07-10

# Options:
# -b: new EPICS base version
# -r: new require version
# -l: new EPICS base root
# -d: update development version
# -p: update production module version

# Process options

while getopts "b:r:l:dpvh" opt; do
  case $opt in
    b)
      base_version="$OPTARG"
      ;;
    r)
      require_version="$OPTARG"
      ;;
    l)
      base_root_new="$OPTARG"
      ;;
    d)
      development_module=true
      ;;
    p)
      production_module=true
      ;;
    v)
      verbose=true
      ;;
    h)
      help=true
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Update base and require versions in configure/RELEASE and/or configure/RELEASE_DEV."
  echo "Recursively searches for all instances of configure/RELEASE and/or configure/RELEASE_DEV"
  echo "and updates the definitions in each instance found."
  echo ""
  echo "usage: ./update-base-require-versions.sh -b <new_base_version> -r <new_require_version> [-l <base_root_orig>] [-n <base_root_new>] [-d] [-p] [-v] [-h]"
  echo "parameters:"
  echo "-b: specify new base version"
  echo "-r: specify new require version"
  echo "-l: specify new location of EPICS base. Update all modules to use this path."
  echo "-d: update development module version"
  echo "-p: update production module version"
  echo "-v: be verbose"
  echo "-h: print this usage information"
fi

if [ "$verbose" = true ]; then
  printf "Argument base_version is %s\n" "$base_version"
  printf "Argument require_version is %s\n" "$require_version"
  printf "Argument base_root_new is %s\n" "$base_root_new"
  if [ "$development_module" = true ]; then
    printf "Argument development_module is true\n"
  else
    printf "Argument development_module is not true\n"
  fi

  if [ "$production_module" = true ]; then
    printf "Argument production_module is true\n"
  else
    printf "Argument production_module is not true\n"
  fi
fi

update_base_version() {
  [ -n "$verbose" ] && echo "release_file = $release_file"
  if [ -z "$base_root_new" ]; then
    # Retain existing base path
    find . -name "$release_file" -print0 | xargs -0 sed -i "s%^EPICS_BASE\(:\?\)=\(.*\)/base-.*%EPICS_BASE\1=\2/base-$base_version%"
  else
    # Update base path
    find . -name "$release_file" -print0 | xargs -0 sed -i "s%^EPICS_BASE\(:\?\)=.*/base-.*%EPICS_BASE\1=$base_root_new/base-$base_version%"
  fi
}

update_require_version() {
  # If base version is not empty, update to the new version
  find . -name "$release_file" -print0 | xargs -0 sed -i "s/E3_REQUIRE_VERSION\(:\?\)=.*/E3_REQUIRE_VERSION\1=$require_version/"
}

export -f update_base_version

if [ "$production_module" = true ]; then
  release_file=RELEASE
  if [ -n "$base_version" ]; then
    update_base_version
  fi
  if [ -n "$require_version" ]; then
    update_require_version
  fi
fi

if [ "$development_module" = true ]; then
  release_file=RELEASE_DEV
  if [ -n "$base_version" ]; then
    update_base_version
  fi
  if [ -n "$require_version" ]; then
    update_require_version
  fi
fi
