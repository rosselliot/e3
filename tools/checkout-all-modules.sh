#!/bin/bash

# Description:
# Script to checkout a tagged verson for all modules in inventory
# Author: Wayne Lewis
# Date: 2020-10-15

# Options:
# -f: input inventory file
# -b: EPICS base version
# -r: require version
# -v: verbose
# -q: quiet
# -h: help

script=$(realpath "$0")
script_path=$(dirname "$script")

# Process options
while getopts "f:b:r:qvh" opt; do
  case $opt in
    f)
      input_file="$OPTARG"
      ;;
    b)
      base_version="$OPTARG"
      ;;
    r)
      require_version="$OPTARG"
      ;;
    v)
      verbose=true
      ;;
    q)
      quiet=true
      ;;
    h)
      help=true
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Check out the latest tagged wrapper version that matches the provided EPICS base and require versions."
  echo "If no matching tags are found, check out the latest version on the default branch."
  echo ""
  echo "usage: ./checkout-all-modules.sh -f <path/to/inventory/file> [-b <epics_base_version>] [-r <require_version>] [-q] [-v] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-b: EPICS base version"
  echo "-r: require version"
  echo "-v: be verbose"
  echo "-q: be quiet"
  echo "-h: print this usage information"
  exit 0
fi

checkout_modules() {
  pwd=$(pwd)
  [[ "$verbose" = true ]] && echo "$pwd"
  [ "$quiet" = true ] && quiet_flag=-q

  # Iterate through the inventory file. # lines are ignored as comments.
  python3 "${script_path}"/get_inventory.py -g all "$input_file" | while IFS= read -r dir; do
    [ "$quiet" = true ] && echo "$dir"
    [[ "$verbose" = true ]] && echo "repo = $dir"
    cd "$dir" || return
    # Make sure we are up to date with the Gitlab repo
    git fetch ess-ssh
    # Handle multiple levels of subgroups
    levels=$(echo "$dir" | tr -cd '/' | wc -c)
    [[ "$verbose" = true ]] && echo "levels = $levels"
    group=$(echo "$dir" | cut -f 1-"$levels" -d'/')
    [ "$verbose" = true ] && echo "subgroup = $group"
    # Search for the selected tag
    tags=$(git for-each-ref --format '%(refname:short)' --sort=creatordate refs/tags)
    [ "$verbose" = true ] && echo "tags = $tags"
    if [ -n "$base_version" ]; then
      [ "$verbose" = true ] && echo "base_version = $base_version"
      tags=$(echo "$tags" | grep "$base_version")
      [ "$verbose" = true ] && echo "tags = $tags"
      if [ -n "$require_version" ]; then
        [ "$verbose" = true ] && echo "require_version = $require_version"
        tags=$(echo "$tags" | grep "$require_version")
        [ "$verbose" = true ] && echo "tags = $tags"
      fi
      tag=$(echo "$tags" | tail -n 1)
      [ "$verbose" = true ] && echo "tag = $tag"
      if [ -n "$tag" ]; then
        git checkout $quiet_flag "$tag"
      else
        echo "No tag found that matches pattern."
      fi
    fi
    cd "$pwd" || return
  done
}

checkout_modules
