#!/bin/bash

# Description:
# Script to create tag for E3 wrapper module
# Author: Wayne Lewis
# Date: 2020-07-13

# Options:
# Process options

while getopts "tpcvh" opt; do
  case $opt in
    t)
      tag_repo=true
      ;;
    p)
      push=true
      ;;
    c)
      check=true
      ;;
    v)
      verbose=true
      ;;
    h)
      help=true
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  echo "Create tag for E3 wrapper module repository"
  echo "usage: ./tag-e3-wrapper [-v] [-h]"
  echo "parameters:"
  echo "-t: tag repository"
  echo "-p: push to remote repository"
  echo "-c: check generated tag against current wrapper"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

generate_tag() {
  # Create the tag components
  local base_version
  base_version=$(grep -e '^EPICS_BASE' configure/RELEASE | awk -F/ '{print $NF}' | sed -e 's/base-//')

  local require_version
  require_version=$(grep -e '^E3_REQUIRE_VERSION' configure/RELEASE | cut -f 2 -d '=')

  local module_version
  module_version=$(grep -e '^E3_MODULE_VERSION' configure/CONFIG_MODULE | cut -f 2 -d '=')

  local git_commit
  git_commit=$(git rev-parse --verify --short HEAD)

  local datestamp
  datestamp=$(date +%Y%m%d)

  local timestamp
  timestamp=$(date +%H%M%S)

  # Assemble the complete tag
  tag="$base_version"-"$require_version"/"$module_version"-"$git_commit"
  if [ ! "$check" = true ]; then
    tag="$tag"-"$datestamp"T"$timestamp"
  fi

  if [ "$verbose" = true ]; then
    echo "base_version = $base_version"
    echo "require_version = $require_version"
    echo "module_version = $module_version"
    echo "git_commit = $git_commit"
    if [ ! "$check" = true ]; then
      echo "datestamp = $datestamp"
      echo "timestamp = $timestamp"
    fi
  fi
}

# Fetch the remote name; prioritise "ess-ssh" in case it exists.
get_remote() {
  remotes=$(git remote)
  if ! echo "$remotes" | grep "^ess-ssh$"; then
    echo "$remotes" | head -n 1
  fi
}

tag_wrapper_module() {
  [ "$verbose" = true ] && echo "Generated tag = $tag"

  # Tag the repository
  if [ "$tag_repo" = true ]; then
    git tag "$tag"
    [ "$verbose" = true ] && echo "Tagged repo with $tag"
  fi

  # Push to the remote
  if [ "$push" = true ]; then
    remote="$(get_remote)"
    [ "$verbose" = true ] && echo "Remote = $remote"
    # Only push if we get a valid response for the remote name
    if [ -n "$remote" ]; then
      if git push "$remote" "$tag"; then
        [ "$verbose" = true ] && echo "Pushed tag '$tag' to $remote"
      else
        echo "Failed to push tag '$tag' to remote. Please make sure you have the correct permissions." >&2
        exit 1
      fi
    fi
  fi
}

check_tag() {
  current_tag="$(git describe --tags)" || exit 1

  if [ "$verbose" = "true" ]; then
    echo "Checking tag:"
    echo "  current tag:   $current_tag"
    echo "  generated tag: $tag"
  fi

  if [[ ! "$current_tag" =~ ^"$tag" ]]; then
    echo "ERROR: Tag mismatch found" >&2
    exit 1
  fi
}

# Check that we are in the right part of the directory tree.
if [[ $PWD = */e3-* ]]; then
  [ "$verbose" = true ] && echo "Current path: $PWD"

  generate_tag
  if [ "$check" = "true" ]; then
    check_tag
  else
    tag_wrapper_module
  fi
else
  echo "Not in an E3 module wrapper directory"
  exit 1
fi
