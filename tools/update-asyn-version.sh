#!/bin/bash

# Description:
# Script to update the asyn version in E3 wrappers
# Author: Wayne Lewis
# Date: 2020-07-14

# Options:
# -a: new asyn version
# -d: update development version
# -p: update production module version

# Process options

while getopts "a:dpvh" opt; do
  case $opt in
    a)
      asyn_version="$OPTARG"
      ;;
    d)
      development_module=true
      ;;
    p)
      production_module=true
      ;;
    v)
      verbose=true
      ;;
    h)
      help=true
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      ;;
  esac
done

if [ "$help" = true ]; then
  # Add usage text here
  echo "usage: ./update-asyn-version.sh -a <new_asyn_version> [-d] [-p] [-v] [-h]"
  echo "parameters:"
  echo "-a: specify new asyn version"
  echo "-d: update development module version"
  echo "-p: update production module version"
  echo "-v: be verbose"
  echo "-h: print this usage information"
  exit 0
fi

if [ "$verbose" = true ]; then
  printf "Argument asyn_version is %s\n" "$asyn_version"
  if [ "$development_module" = true ]; then
    printf "Argument development_module is true\n"
  else
    printf "Argument development_module is not true\n"
  fi

  if [ "$production_module" = true ]; then
    printf "Argument production_module is true\n"
  else
    printf "Argument production_module is not true\n"
  fi
fi

update_asyn_version() {
  [ -n "$verbose" ] && echo "config_file = $config_file"
  find . -name "$config_file" -print0 | xargs -0 sed -i "s/ASYN_DEP_VERSION\(:\?\)=.*$/ASYN_DEP_VERSION\1=$asyn_version/"
}

export -f update_asyn_version

if [ "$production_module" = true ]; then
  config_file=CONFIG_MODULE
  if [ -n "$asyn_version" ]; then
    update_asyn_version
  fi
fi

if [ "$development_module" = true ]; then
  config_file=CONFIG_MODULE_DEV
  if [ -n "$asyn_version" ]; then
    update_asyn_version
  fi
fi
