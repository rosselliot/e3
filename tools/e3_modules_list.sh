#!/usr/bin/env bash
# THIS FILE IS REQUIRED FOR ./../e3.bash AND ./../e3_building_config.bash TO WORK
#

declare -r inventory_file="${SC_TOP}/tools/e3-inventory.yaml"

common=""
ts=""
psi=""
ifc=""
ecat=""
area=""
ps=""
vac=""
rf=""
bi=""
mps=""
only=""

options=ctpieasvlbmo

function usage_title {
  echo ""
  echo "Usage    : $0 [ -${options} ] <option> "
  echo ""
}

function usage_mod {
  echo ""
  echo "           -c     : common       : epics modules"
  echo "           -t     : ts           : mrf timing modules"
  echo "           -p{c}  : psi          : PSI modules"
  echo "           -i{c}  : ifc          : ifc modules"
  echo "           -e{c}  : ecat         : ethercat modules"
  echo "           -a{c}  : area         : area detector modules / BI Modules"
  echo "           -s{c}  : ps           : Power supply modules"
  echo "           -v{c}  : vac          : Vacuum modules"
  echo "           -l{c}  : rf           : RF modules"
  echo "           -b{ca} : bi           : beam instrumentation modules (based on AD)"
  echo "           -m{c}  : mps          : MPS modules"
  echo "           {c,ci} : dep modules  : enable by default if not defined (dependent modules)"
  echo "             -o   : only         : ignore dependent modules"
  echo "                                   the option -e is actually -ec. And -eo means -e."
  echo ""
  echo ""
}

list_arg=()
function module_list_func {
  [ -n "${common}" ] && list_arg+=("-g" "common")
  [ -n "${ts}" ] && list_arg+=("-g" "ts")
  [ -n "${psi}" ] && list_arg+=("-g" "psi")
  [ -n "${ifc}" ] && list_arg+=("-g" "ifc")
  [ -n "${ecat}" ] && list_arg+=("-g" "ecat")
  [ -n "${area}" ] && list_arg+=("-g" "area")
  [ -n "${ps}" ] && list_arg+=("-g" "ps")
  [ -n "${vac}" ] && list_arg+=("-g" "vac")
  [ -n "${rf}" ] && list_arg+=("-g" "rf")
  [ -n "${bi}" ] && list_arg+=("-g" "bi")
  [ -n "${mps}" ] && list_arg+=("-g" "mps")
  [ -n "${only}" ] && list_arg+=("-o")

  # The following is used in e3.bash:
  # shellcheck disable=SC2034
  mapfile -t module_list <<<"$(python3 "${SC_TOP}"/tools/get_inventory.py "${list_arg[@]}" "${inventory_file}")"
}
