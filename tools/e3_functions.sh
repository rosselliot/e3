# shellcheck shell=bash
#
# THIS FILE IS REQUIRED FOR ./../e3.bash AND ./../e3_building_config.bash TO WORK
#

#  Copyright (c) 2018 - 2019  Jeong Han Lee
#  Copyright (c) 2017 - 2021  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt

function die {
  error=${1:-1}
  ## exits with 1 if error number not given
  shift
  [ -n "$*" ] &&
    printf "%s: %s\n" "$SC_SCRIPTNAME" "$*" >&2
  exit "$error"
}

# TODO: This only works if we are using require 3.3.0 or later. If an earlier
#       version is used, we need a line such as
#
#       grep -qE "^\s*E3_MODULE_SRC_PATH:?=.*-loc$" configure/CONFIG_MODULE
function is_local {
  ! grep -qE "^\s*EPICS_MODULE_TAG:?=" configure/CONFIG_MODULE
}

# Usage :
# e3_version="$(read_file_get_string  "${file_name}" "E3_VERSION:=")";
function read_file_get_string {
  local FILENAME=$1
  local PREFIX=$2

  sed -n "s/^$PREFIX\(.*\)/\1/p" "$FILENAME"
}

function print_module_list {
  local a_list
  local i
  echo ">> Vertical display for the selected modules :"
  echo ""
  ((i = 0))
  printf " Modules List \n"
  # shellcheck disable=SC2154
  for a_list in "${module_list[@]}"; do
    printf "  %3d : %s\n" "$i" "$a_list"
    ((++i))
  done
  echo ""
}

# This will clone a repository within the e3 tree and attempt to check out the latest tagged commit.
function git_clone {
  local rep_path="$1"
  local rep_name
  rep_name=$(basename "$rep_path")
  local tag
  shift
  printf ">> %s\n" "$rep_name"
  if [[ -d "${rep_name}" ]]; then
    printf ">> Target directory is found, Pulling instead of Cloning ... \n\n"
    pushd "${rep_name}" || die
    git pull || die 1 "${FUNCNAME[*]} : Pulling Error at ${rep_path}: Please check the repository status"
    popd || die
  else
    printf ">> No target directory is found, Cloning...\n\n"
    ${GIT_CMD} "${GIT_URL}"/"${rep_path}".git || die 1 "${FUNCNAME[*]} : Cloning Error at ${rep_path}: Please check the repository status"
    pushd "${rep_name}" || die
    if git describe --tags; then
      tag="$(git describe --tags --abbrev=0)"
      printf ">> Checking out latest tag: %s\n" "$tag"
      git checkout "$tag"
    else
      printf ">> Warning: no tags found for %s. Repository will be in left in 'master' state.\n" "${rep_name}" >&2
    fi
    popd || die
  fi
  printf "\n"
}

function git_cmd {
  local rep=""
  local git_args=("$@")
  local i
  ((i = 0))
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      printf "\n>> $i : %s : %s %s \n" "${rep}" "${FUNCNAME[*]}" "${git_args[*]}"
      git "${git_args[@]}"
      popd || die
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
    ((++i))
  done
}

function git_commit {
  git_cmd commit -m "$1"
}

function git_push {
  git_cmd push
}

function git_status {
  git_cmd status
}

function git_add {
  git_cmd add "$@"
}

function git_rm {
  git_cmd rm "$@"
}

function git_diff {
  git_cmd diff "$@"
}

function git_checkout {
  git_cmd checkout -- "$@"
}

function git_pull_modules {
  git_cmd pull
}

function git_pull_all {
  local rep=""

  # shellcheck disable=SC2154
  for rep in "${base_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      git pull
      popd || die
    fi
  done

  rep=""
  # shellcheck disable=SC2154
  for rep in "${require_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      git pull
      popd || die
    fi
  done

  git_pull_vendor_modules
  git_pull_modules
}

function print_version_info_base {
  local rep=""
  local conf_mod="configure/CONFIG_BASE"
  local epics_base_tag=""
  local e3_base_version=""
  local e3_base_path=""
  local epics_base=""

  for rep in "${base_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      if [[ -f "${conf_mod}" ]]; then
        epics_base_tag="$(read_file_get_string "${conf_mod}" "EPICS_BASE_TAG:=")"
        e3_base_version="$(read_file_get_string "${conf_mod}" "E3_BASE_VERSION:=")"
        e3_base_path="$(read_file_get_string "${conf_mod}" "E3_EPICS_PATH:=")"
        epics_base="${e3_base_path}/base-${e3_base_version}"
      fi
      printf "\n"
      printf ">> %s\n" "${rep}"
      printf "   EPICS_BASE      : %s\n" "${epics_base}"
      printf "   EPICS_BASE_TAG  : %s\n" "${epics_base_tag}"
      printf "   E3_BASE_VERSION : %s\n" "${e3_base_version}"
      branch_info=$(git rev-parse --abbrev-ref HEAD)
      printf "   E3 Branch       : %s\n" "${branch_info}"
      tag_info=$(git tag --points-at HEAD)
      printf "   E3 Tag          : %s\n" "${tag_info}"
      popd || die
    fi
  done
}

function print_version_info_require {
  local rep=""
  local conf_mod="configure/CONFIG_MODULE"
  local epics_module_tag=""
  local e3_version=""
  local release_info="configure/RELEASE"
  local e3_base_path=""

  for rep in "${require_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      if [[ -f "${conf_mod}" ]]; then
        epics_module_tag="$(read_file_get_string "${conf_mod}" "EPICS_MODULE_TAG:=")"
        e3_version="$(read_file_get_string "${conf_mod}" "E3_MODULE_VERSION:=")"
      fi
      if [[ -f "configure/RELEASE" ]]; then
        e3_base_path="$(read_file_get_string "${release_info}" "EPICS_BASE=")"
      fi
      printf "\n"
      printf ">> %s\n" "${rep}"
      printf "   EPICS_BASE        : %s\n" "${e3_base_path}"
      printf "   EPICS_MODULE_TAG  : %s\n" "${epics_module_tag}"
      printf "   E3_MODULE_VERSION : %s\n" "${e3_version}"
      branch_info=$(git rev-parse --abbrev-ref HEAD)
      printf "   E3 Branch         : %s\n" "${branch_info}"
      tag_info=$(git tag --points-at HEAD)
      printf "   E3 Tag            : %s\n" "${tag_info}"
      popd || die
    fi
  done
}

function print_installed_modules {
  local prefix="$1"
  shift
  local targets=("$@")
  local rep=""
  local module_name

  for rep in "${targets[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" &>/dev/null || die
      make "$prefix"existent LEVEL=1 | head -n -2
      popd &>/dev/null || die
    else
      if [[ -n "${E3_SITEMODS_PATH}" ]]; then
        # shellcheck disable=SC1003
        module_name=$(echo "${rep##*e3-}" | awk '{print tolower($0)}')
        tree -d -L 1 "${E3_SITEMODS_PATH}/${module_name}" | head -n -2
      else
        printf "%s has not been cloned and cannot be found on your PATH. Ignoring." "${rep}"
      fi
    fi
  done
}

function print_version_info_modules {
  local rep=""
  local conf_mod="configure/CONFIG_MODULE"
  local epics_module_tag=""
  local e3_version=""
  local e3_version=""
  local release_info="configure/RELEASE"
  local e3_base_path=""
  local e3_require_version=""
  local e3_require_name=""

  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      if [[ -f "configure/CONFIG_MODULE" ]]; then
        epics_module_tag="$(read_file_get_string "${conf_mod}" "EPICS_MODULE_TAG:=")"
        e3_version="$(read_file_get_string "${conf_mod}" "E3_MODULE_VERSION:=")"
      fi
      if [[ -f "configure/RELEASE" ]]; then
        e3_base_path="$(read_file_get_string "${release_info}" "EPICS_BASE=")"
        e3_require_version="$(read_file_get_string "${release_info}" "E3_REQUIRE_NAME:=")"
        e3_require_name="$(read_file_get_string "${release_info}" "E3_REQUIRE_VERSION:=")"
      fi
      printf "\n"
      printf ">> %s\n" "${rep}"
      printf "   EPICS_BASE         : %s\n" "${e3_base_path}"
      printf "   E3_REQUIRE_NAME    : %s\n" "${e3_require_version}"
      printf "   E3_REQUIRE_VERSION : %s\n" "${e3_require_name}"
      printf "   EPICS_MODULE_TAG   : %s\n" "${epics_module_tag}"
      printf "   E3_MODULE_VERSION  : %s\n" "${e3_version}"
      branch_info=$(git rev-parse --abbrev-ref HEAD)
      printf "   E3 Branch          : %s\n" "${branch_info}"
      tag_info=$(git tag --points-at HEAD)
      printf "   E3 Tag             : %s\n" "${tag_info}"
      popd || die
    fi
  done
}

function print_version_info_all {
  print_version_info_base
  print_version_info_require
  print_version_info_modules
}

function print_version_info_e3 {
  local rep=${SC_TOP}
  if [[ -d "${rep}" ]]; then
    pushd "${rep}" || die
    printf ">> %s\n" "${rep}"
    branch_info=$(git rev-parse --abbrev-ref HEAD)
    printf "   E3 Branch         : %s\n" "${branch_info}"
    tag_info=$(git tag --points-at HEAD)
    printf "   E3 Tag            : %s\n" "${tag_info}"
    printf "   E3 Release        : \n"
    tree -L 2 release_*
    popd || die
  fi
}

function print_version_really_everything {
  local rep=""
  shift
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      make vars
      popd || die
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
  done
}

function exec_makefile_rule {
  local rep=""
  local options=$1
  shift
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      printf "<<<<< ---- %20s: %6s ---- <<<<<<\n" "$rep" "${options}"
      make --no-print-directory -s -C "${rep}" "${options}"
      printf ">>>>> ---- %20s: %6s ---- >>>>>>\n\n" "$rep" "${options}"
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
  done
}

function cat_file {
  local rep=""
  local cat_a_file=$1
  shift
  local i
  ((i = 0))
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      printf "\n>> $i : %s : %s %s \n" "${rep}" "${FUNCNAME[*]}" "${cat_a_file}"
      cat -bns "${cat_a_file}"
      popd || die
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
    ((++i))
  done
}

# BASE
function clone_base {
  local rep=""
  for rep in "${base_list[@]}"; do
    git_clone "${rep}" || die 1 "${FUNCNAME[*]} : git clone ERROR at ${rep}: the target ${rep} may be exist, Please check it"
  done
}

# BASE
function clean_base {
  local rep=""
  for rep in "${base_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      echo "Cleaning .... $rep"
      set -x
      rm -rf "${SC_TOP:?must not be null}"/"${rep}" || die 1 "${FUNCNAME[*]} : one needs the sudo permission to do this. Pleases sudo rm -rf ${SC_TOP}/${rep}"
      set +x
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
  done
}

# BASE
function git_checkout_base {
  local checkout_target=$1
  shift

  if [[ -n "${checkout_target}" ]]; then
    local rep=""
    for rep in "${base_list[@]}"; do
      if [[ -d "${rep}" ]]; then
        pushd "${rep}" || die
        git checkout "${checkout_target}"
        popd || die
      else
        printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
      fi
    done
  else
    printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
  fi
}

## REQUIRE
function clone_require {
  local rep=""
  local latest_tag=""
  for rep in "${require_list[@]}"; do
    git_clone "${rep}" || die 1 "${FUNCNAME[*]} : git clone ERROR at ${rep}: the target ${rep} may be exist, Please check it"
    pushd "${rep}" || die
    # shellcheck disable=SC2154
    if [ -n "$require_version" ]; then
      printf " %20s: Checking out %s version '%s'\n" "${FUNCNAME[*]}" "${rep}" "${require_version}"
      git checkout "$require_version"
    else
      latest_tag="$(git describe --tags --abbrev=0)"
      printf " %20s: Checking out %s version '%s'\n" "${FUNCNAME[*]}" "${rep}" "${latest_tag}"
      git checkout "$latest_tag"
    fi
    popd || die
  done
}

## REQUIRE
function clean_require {
  local rep=""
  for rep in "${require_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      make uninstall
      popd || die
      set -x
      rm -rf "${SC_TOP:?must not be null}"/"${rep}" || die 1 "${FUNCNAME[*]} : one needs the sudo permission to do this. Pleases sudo rm -rf ${SC_TOP}/${rep}"
      set +x
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
  done
}

## REQUIRE
function git_checkout_require {
  local checkout_target=$1
  shift
  if [[ -n "${checkout_target}" ]]; then
    local rep=""
    for rep in "${require_list[@]}"; do
      if [[ -d "${rep}" ]]; then
        pushd "${rep}" || die
        git checkout "${checkout_target}"
        popd || die
      else
        printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
      fi
    done
  else
    printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
  fi
}

function clean_vendor_modules {
  local rep=""
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      echo "Cleaning .... $rep"
      pushd "${rep}" || die
      make uninstall
      popd || die
      set -x
      rm -rf "${SC_TOP:?must not be null}"/"${rep}" || die 1 "${FUNCNAME[*]} : one needs the sudo permission to do this. Pleases sudo rm -rf ${SC_TOP}/${rep}"
      set +x
    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
    printf "\n"
  done
}

## MODULES
function clone_modules {
  local rep=""
  local base_dir=""
  for rep in "${module_list[@]}"; do
    base_dir=$(dirname "$rep")
    mkdir -p "$base_dir"
    pushd "$base_dir" || die
    git_clone "${rep}" || die 1 "${FUNCNAME[*]} : git clone ERROR at ${rep}: the target ${rep} may be exist, Please check it"
    popd || die
  done
}

## MODULES
function git_checkout_modules {
  local checkout_target=$1
  shift
  if [[ -n "${checkout_target}" ]]; then
    local rep=""
    for rep in "${module_list[@]}"; do
      if [[ -d "${rep}" ]]; then
        pushd "${rep}" || die
        git checkout "${checkout_target}"
        popd || die
      else
        printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
      fi
    done
  else
    printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
  fi
}

function clean_modules {
  local rep=""

  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      echo "Cleaning .... $rep"
      pushd "${rep}" || die
      make uninstall
      popd || die
      set -x
      rm -rf "${SC_TOP:?must not be null}"/"${rep}" || die 1 "${FUNCNAME[*]} : one needs the sudo permission to do this. Pleases sudo rm -rf ${SC_TOP}/${rep}"
      set +x

    else
      printf " %20s: SKIP %20s, we cannot find it\n" "${FUNCNAME[*]}" "${rep}"
    fi
    printf "\n"
  done

}

function clone_all {
  clone_base
  clone_require
  clone_modules
}

function clean_all {
  clean_modules
  clean_require
  clean_base
}

function git_checkout_all {
  local checkout_target=$1
  shift
  if [[ -n "${checkout_target}" ]]; then
    git_checkout_base "${checkout_target}"
    git_checkout_require "${checkout_target}"
    git_checkout_modules "${checkout_target}"
  else
    printf " %20s: SKIP all checkout, we cannot find it\n" "${FUNCNAME[*]}"
  fi
}

## Must be called in where ${TOP}/e3-*
## Assumption : To call this function is that it is OK to reset all in
## Reset all local changes : git checkout --force
## arg 1 : module name
## arg 2 : version (hash, branch, ...)
function rebuild_module {
  local module_name=$1
  shift
  local module_version=$1
  shift
  local module_path_name=""

  if [[ -z "${module_name}" ]]; then
    printf "Module name is mandatory\n"
    exit
  else
    module_path_name=e3-${module_name}
  fi

  if [[ ! -d "${module_path_name}" ]]; then
    printf "%s doesn't exist, Cloning ....\n" "${module_path_name}"
    git_clone "${module_path_name}"
  fi

  pushd "${module_path_name}" || die

  git fetch --all
  if [[ -n "${module_version}" ]]; then
    git checkout -f "${module_version}" || die 1 "${FUNCNAME[*]} : there is no ${module_version} in ${module_name}. Please check it"
  fi
  make init || die 1 "${FUNCNAME[*]} : make init in ${module_name} has error. Please check it"
  make patch || die 1 "${FUNCNAME[*]} : make patch in ${module_name} has error. Please check it"
  make rebuild || die 1 "${FUNCNAME[*]} : cannot rebuild ${module_name}. Please check it"

  popd || die
}
