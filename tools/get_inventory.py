#!/usr/bin/env python3
import argparse
import sys

import yaml


def resolve_dependent_groups(data, groups):
    """
    Fetches all groups on which your group depends

    :param data: dictionary containing module dependency information
    :param groups: list of groups whose dependencies we are seeking
    """
    selected = set()
    for group in groups:
        selected.update(
            [group, *resolve_dependent_groups(data, data[group]["depends"])]
        )
    return selected


def get_group_modules(data, groups, only):
    """
    Fetches all modules within a list of groups

    :param data: dictionary containing module dependency information
    :param groups: list of groups whose modules we are seeking
    :param only: if true, only fetch the groups from this module instead of all dependent modules
    """
    if "all" in groups:
        selected = data.keys()
    elif only:
        selected = groups
    else:
        selected = resolve_dependent_groups(data, groups)

    # We want to make sure we take the groups in the order they occur in the inventory file
    modules = []
    for group in filter(lambda x: x in selected, data):
        modules.extend([f"{group}/e3-{module}" for module in data[group]["modules"]])
    return "\n".join(modules)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("inventory_file")
    parser.add_argument("-g", "--group", action="append")
    parser.add_argument("-o", "--only", action="store_true")

    args = parser.parse_args()

    try:
        with open(args.inventory_file, "r") as f:
            data = yaml.safe_load(f)
    except FileNotFoundError:
        print(
            f"ERROR: Inventory file '{args.inventory_file}' not found", file=sys.stderr
        )
        sys.exit(1)

    if args.group:
        out = get_group_modules(data, args.group, args.only)
    else:
        out = "\n".join(data)

    print(out)


if __name__ == "__main__":
    main()
