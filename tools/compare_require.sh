#!/usr/bin/env bash

TOP="$(pwd)"
readonly TOP
tools_dir="$(cd "$(dirname "$0")" && pwd)"
readonly tools_dir

die() {
  echo "$1"
  exit 1
}

usage() {
  echo "Usage: bash $0 [-b base_path] [-f inventory_file] [-g group] ... [-o output_file] [-c] tag1 tag2"
  echo "   -b: specify EPICS base path (default: /epics/base-7.0.4)"
  echo "   -f: inventory file"
  echo "   -g: group to build (-g all will build all groups)"
  echo "   -o: output file. If omitted, will display to terminal"
  echo "   -c: cleanup temporary files"
  echo ""
  echo "   tag1 tag2: compare these two tags in require repo"
}

local_module_version() {
  [[ -n "$1" ]] || die "Tag must be non-empty"
  cat >configure/CONFIG_MODULE.local <<EOF
#EPICS_MODULE_TAG:=$1
E3_MODULE_VERSION:=$1_tmp
EOF
}

compare() {
  echo "Comparing require verisons $1 and $2"
  diff -qr --exclude=*.so --exclude=*.yaml "$base_path/require/$1_tmp" "$base_path/require/$2_tmp"
  echo "======================================================="
  for d in "$base_path/require/$1_tmp/siteMods"/*/*; do
    v=$(basename "$d")
    m=$(basename "$(dirname "$d")")
    s1="$d/lib/linux-x86_64/lib${m}.so"
    s2="$base_path/require/$2_tmp/siteMods/$m/$v/lib/linux-x86_64/lib${m}.so"
    if [[ -f "$s1" && -f "$s2" ]]; then
      echo "  Comparing lib${m}.so"
      echo "  ========"
      diff <(xxd "$s1") <(xxd "$s2")
      echo "  ========"
    fi
  done
}

cleanup() {
  path="$base_path/require/$1_tmp"
  [[ -d "$path" ]] && rm -rf "$path"
}

groups=()

DEFAULT_BASE_VERSION=7.0.4
base_path=/epics/base-$DEFAULT_BASE_VERSION
while getopts "b:f:g:o:ch" opt; do
  case "$opt" in
    b)
      base_path="$OPTARG"
      ;;
    f)
      inventory_file="$OPTARG"
      ;;
    g)
      groups+=("$OPTARG")
      ;;
    o)
      output_file="$OPTARG"
      ;;
    c)
      cleanup=true
      ;;
    h)
      usage
      exit 0
      ;;
    \?)
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))

# Allow for "-g all" to build all of the modules
if printf '%s\n' "${groups[@]}" | grep -q "^all$"; then
  mapfile -t groups <<<"$(python3 "$tools_dir/get_inventory.py" "$inventory_file")"
fi

if [[ $# -lt 2 ]]; then
  usage
  exit 1
fi

tag1="$1"
tag2="$2"

[[ -d "$base_path" ]] || die "Invalid base_path: $base_path"
[[ -f "$inventory_file" ]] || die "Invalid inventory file: $inventory_file"

tmp_require_dir="require_tmp"

git clone --recursive git@gitlab.esss.lu.se:e3/e3-require.git "$tmp_require_dir" || die "git clone failed"
pushd "$tmp_require_dir" || exit 1

# Clone the modules only once
mkdir -p modules_tmp
pushd modules_tmp || exit 1
for group in "${groups[@]}"; do
  bash "$tools_dir/clone-group-modules.sh" -f "$TOP/$inventory_file" -g "$group" -v
done
popd || exit 1

# Iterate over all of the tags.
short_tags=()
for tag in $tag1 $tag2; do
  git checkout "$tag" || die "Cannot checkout '$tag' from e3-require"
  short_tag=$(git rev-parse --short HEAD)
  short_tags+=("$short_tag")
  local_module_version "$short_tag"

  make init || die "Make init (tag $tag) failed"
  make patch || die "Make patch (tag $tag) failed"
  make build || die "Make build (tag $tag) failed"
  make install || die "Make install (tag $tag) failed"

  mkdir -p modules_tmp

  # Override E3_REQUIRE_VERSION for each of the modules
  pushd modules_tmp >/dev/null || exit 1
  cat >RELEASE.local <<EOF
E3_REQUIRE_VERSION:=${short_tag}_tmp
EOF

  # Build!
  for group in "${groups[@]}"; do
    bash "$tools_dir/build-group-modules.sh" -f "$TOP/$inventory_file" -g "$group" -v -i -s
  done
  popd >/dev/null || exit 1
done

# Compare the results:
if [ -n "$output_file" ]; then
  compare "${short_tags[0]}" "${short_tags[1]}" >../"$output_file"
else
  compare "${short_tags[0]}" "${short_tags[1]}"
fi

if [[ "$cleanup" = true ]]; then
  for tag in "${short_tags[@]}"; do
    echo "Removing require version $tag..."
    cleanup "$tag"
  done
  popd || exit 1
  rm -rf "$tmp_require_dir"
fi
