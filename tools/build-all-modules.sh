#!/bin/bash

# Description:
# Script to build all module groups
# Author: Wayne Lewis
# Date: 2020-07-15

# Options:
# -d: build development version
# -f: input inventory file
# -e: abort on error flag
# -i: install flag
# -v: verbose
# -h: help

### test

help() {
  echo "Build all modules whose path matches a defined gitlab subgroup name."
  echo ""
  echo "usage: ./build-all-modules.sh -f <path/to/inventory/file> [-e] [-i] [-v] [-o] [-h]"
  echo "parameters:"
  echo "-f: specify inventory file path"
  echo "-e: abort on build errors"
  echo "-i: install the built module"
  echo "-v: be verbose"
  echo "-o: fix group ownership"
  echo "-h: print this usage information"
}

# Process options
while getopts "f:deivoh" opt; do
  case $opt in
    f)
      input_file="$OPTARG"
      ;;
    d)
      development=true
      ;;
    e)
      abort_on_error=true
      ;;
    i)
      install=true
      ;;
    v)
      verbose=true
      ;;
    o)
      ownership=true
      ;;
    h)
      help
      exit 0
      ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      help
      exit 1
      ;;
  esac
done

build_modules() {

  script=$(realpath "$0")
  script_path=$(dirname "$script")

  [[ "$install" = true ]] && install_flag=-i
  [[ "$verbose" = true ]] && verbose_flag=-v
  [[ "$abort_on_error" = true ]] && abort_on_error_flag=-e
  [[ "$development" = true ]] && development_flag=-d
  [[ "$ownership" = true ]] && ownership_flag=-o

  mapfile -t group_names <<<"$(python3 "$script_path/get_inventory.py" "$input_file")"
  for group_name in "${group_names[@]}"; do
    [ "$verbose" = true ] && echo "Group = $group_name"
    bash -c "$script_path/build-group-modules.sh -f $input_file -g $group_name $install_flag $verbose_flag $abort_on_error_flag $development_flag $ownership_flag" ||
      { [[ "$abort_on_error" = true ]] && echo ">>>> Build failed for group $group_name" >&2 && break; }
  done

}

[ ! -f "$input_file" ] && echo "Input file '$input_file' does not exist." >&2 && exit 1

build_modules
