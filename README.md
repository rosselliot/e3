![alt text](docs/img/logo-slim.png "e3")

This repository contains the setup and maintenance scripts for European Spallation Source ERIC's EPICS environment, e3.

For a project history, see the [CHANGELOG.md](CHANGELOG.md).

## Documentation

Documentation is hosted using GitLab Pages: <http://e3.pages.esss.lu.se>.

## Getting started

e3 supports using multiple EPICS environments, and easily switching between these.

### Prerequisites

e3 is primarily built for CentOS 7, but should also work for Debian and Ubuntu.

#### CentOS 7

```sh
$ sudo yum install -y \
git tree ipmitool autoconf libtool automake m4 re2c tclx \
coreutils graphviz patch readline-devel libXt-devel libXp-devel libXmu-devel \
libXpm-devel lesstif-devel gcc-c++ ncurses-devel perl-devel net-snmp net-snmp-utils \
net-snmp-devel libxml2-devel libpng12-devel libzip-devel libusb-devel \
python-devel darcs hdf5-devel boost-devel pcre-devel opencv-devel \
libcurl-devel blosc-devel libtiff-devel libjpeg-turbo-devel \
libusbx-devel systemd-devel libraw1394.x86_64 hg libtirpc-devel \
liberation-fonts-common liberation-narrow-fonts \
liberation-mono-fonts liberation-serif-fonts liberation-sans-fonts \
logrotate xorg-x11-fonts-misc cpan kernel-devel symlinks \
dkms procServ curl netcdf netcdf-devel patchelf python3-devel \
boost-devel glib2-devel
```

There are also some python3 packages that are necessary:
```sh
$ pip3 install --user pyyaml
```

### Installing

For a local installation of e3, the utilities `e3_building_config.bash` and `e3.bash` can be used. To install EPICS base and [*require*](https://gitlab.esss.lu.se/e3/e3-require/):

```sh
$ ./e3_building_config -t /opt/epics -b 7.0.5 -r 3.4.0 setup
$ ./e3.bash base
$ ./e3.bash req
```

To install an e3 module/application/library:

* clone the wrapper

* modify `configure/RELEASE` in the e3-wrapper to specify the version of EPICS base and *require* that you want to use, and where these can be found (e.g. `EPICS_BASE=/opt/epics/base-7.0.4`, `E3_REQUIRE_VERSION=3.3.0`)

```sh
$ cd e3-module_name
$ make init patch build
$ sudo make install
```

There are also module groups that can be installed;

```sh
$ ./e3.bash -c mod  # for the "common" group
```

You can find the module groups here on gitlab: <https://gitlab.esss.lu.se/e3>. You can also see available groups by running `./e3.bash` (without any arguments), and to find out what is in a group, specify the group and add the argument `vars`; i.e., for the *common* group it would be `./e3.bash -c vars`.

> **N.B.!** Beware that what `e3.bash` does is to clone the `master` branch of base, require, and all listed modules. You will often want to check out specific (usually tagged) commits instead.

### Usage

You load an EPICS environment in a shell by sourcing a file called `setE3Env.bash` in your installed directory tree:

```sh
$ source /opt/epics/base-7.0.5/require/3.4.0/bin/setE3Env.bash
```

You can then finally load an IOC shell by running `iocsh.bash <args>`.

## Contributing

Contributions through pull/merge requests only.
