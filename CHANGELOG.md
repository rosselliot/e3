# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Many bugfixes
- Added Ubuntu 18.04 to gitlab CI build
- Fixes to `e3.bash` and associated utils
- Improved gitlab-CI, including that it now also builds and installs e3-autosave as well as the "common" module group
- Significant DRY-type cleanup, removed a lot of redundant code and cleaned functionality up
- When cloning a repository with e3.bash (e.g. `bash e3.bash -c gmod)` or `bash e3.bash greq`, etc), the latest
  tagged commit will be checked out if it exists, instead of the master branch. This allows for more consistent
  replication in case of new changes in the wrapper repositories.
- Add `patchelf` to prereqs - required by `opcua`
- Modifies inventory quite significantly
- Changes `tools/protect_branches.py` to `tools/protect_repos.py`; now also protects a tag
- Updated default base/require to 7.0.5/3.4.0 in `e3_building_config.bash`
- Added an option for `tag-e3-wrapper.sh` to check that the tag matches the current config
- Replaced `e3-inventory.txt` with `e3-inventory.yaml`, which contains some metadata regarding dependencies.
  Also added `get_inventory.py` to parse this and return information about groups and modules within those
  groups.

## [0.1.0] - 2021-01-12
- First tagged version of this repository
- Incorporates all changes made up to this date
- See https://jira.esss.lu.se/browse/E3-26 for more information


[Unreleased]: https://gitlab.esss.lu.se/e3/e3/compare/0.1.0...HEAD
[0.1.0]: https://gitlab.esss.lu.se/e3/e3/-/tags/0.1.0
