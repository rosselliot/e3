#!/usr/bin/env bash
#
#  Copyright (c) 2018 - 2019  Jeong Han Lee
#  Copyright (c) 2018 - 2021  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Monday, November 25 22:01:38 CET 2019
# version : 0.2.5

SC_SCRIPT="$(realpath "$0")"
declare -gr SC_SCRIPT
declare -gr SC_SCRIPTNAME="${0##*/}"
declare -gr SC_TOP="${SC_SCRIPT%/*}"

declare -g TARGET=""
declare -g BASE_VERSION=""
declare -g REQUIRE_VERSION=""
declare -g BASE_TAG=""

declare -gr DEFAULT_TARGET_PATH="/epics"
declare -gr DEFAULT_BASE_VERSION="7.0.5"
declare -gr DEFAULT_REQ_VERSION="3.4.0"
declare -gr DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_PATH="/opt/ifc14xx"
declare -gr DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_VER="2.6-4.14"
declare -gr DEFAULT_E3_CC_POKY_TOOLCHAIN_PATH="/opt/cct"
declare -gr DEFAULT_E3_CC_POKY_TOOLCHAIN_VER="2.6-4.14"

# shellcheck source=tools/e3_functions.sh
. "${SC_TOP}"/tools/e3_functions.sh

function require_code_generator { #@ Generator REQUIRE version
  #@ USAGE: REQ_CODE=$(require_code_generator ${require_version})

  local require_full_version=$1
  shift

  local require_version
  require_version=$(echo "$require_full_version" | grep -o '[^-]*$')
  local require_ver_major
  require_ver_major=$(echo "$require_version" | cut -d. -f1)
  local require_ver_minor
  require_ver_minor=$(echo "$require_version" | cut -d. -f2)
  local require_ver_patch
  require_ver_patch=$(echo "$require_version" | cut -d. -f3)

  local req_code=""

  if [[ ${#require_ver_major} -lt 2 ]]; then
    require_ver_major="00${require_ver_major}"
    require_ver_major="${require_ver_major: -2}"
  fi

  if [[ ${#require_ver_minor} -lt 2 ]]; then
    require_ver_minor="00${require_ver_minor}"
    require_ver_minor="${require_ver_minor: -2}"
  fi

  if [[ ${#require_ver_patch} -lt 2 ]]; then
    require_ver_patch="00${require_ver_patch}"
    require_ver_patch="${require_ver_patch: -2}"
  fi

  req_code=${require_ver_major}${require_ver_minor}${require_ver_patch}
  echo "$req_code"
}

function yes_or_no_to_go {
  printf ">> \n"
  printf "  The following configuration for e3 installation\n"
  printf "  will be generated :\n"

  print_options

  printf "\n"
  read -r -p ">> Do you want to continue (y/N)? " answer
  case ${answer:0:1} in
    y | Y)
      printf "\n"
      ;;
    *)
      printf ">> Stop here. \n"
      exit
      ;;
  esac
}

function usage {
  {
    echo ""
    echo "Usage    : $0 options setup "
    echo ""
    echo "              possbile options"
    echo ""
    echo "              [-t <target_path>]     [-b <base_version>]      [-r <require_version>] [-c <base_tag>] "
    echo "              [-n <ifc14xx_cc_path>] [-m <ifc14xx_cc_version] [-l <poky_cc_path]     [-k <poky_cc_version>] "
    echo ""
    echo "               -t : default ${DEFAULT_TARGET_PATH}"
    echo "               -b : default ${DEFAULT_BASE_VERSION}"
    echo "               -r : default ${DEFAULT_REQ_VERSION}"
    echo "               -c : default ${DEFAULT_BASE_VERSION}"
    echo "               -n : default ${DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_PATH}"
    echo "               -m : default ${DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_VER}"
    echo "               -l : default ${DEFAULT_E3_CC_POKY_TOOLCHAIN_PATH}"
    echo "               -k : default ${DEFAULT_E3_CC_POKY_TOOLCHAIN_VER}"
    echo ""
    echo " bash $0 -t \${HOME} -r ${DEFAULT_REQ_VERSION} setup"
    echo ""
  } 1>&2
  exit 1
}

function print_options {
  printf "\n"
  printf ">> Set the global configuration as follows:\n"
  printf ">>\n"
  printf "  EPICS TARGET                     : %s\n" "${TARGET}"
  printf "  EPICS_BASE                       : %s\n" "${EPICS_BASE_TARGET}"
  printf "  EPICS_BASE VERSION               : %s\n" "${BASE_VERSION}"
  printf "  EPICS_MODULE_TAG                 : %s\n" "${BASE_TAG}"
  printf "  E3_REQUIRE_VERSION               : %s\n" "${REQUIRE_VERSION}"
  printf "  E3_REQUIRE_LOCATION              : %s\n" "${REQUIRE_TARGET}"
  printf "  E3_CC_IFC14XX_TOOLCHAIN_PATH     : %s\n" "${IFC14XX_TOOLCHAIN_PATH}"
  printf "  E3_CC_IFC14XX_TOOLCHAIN_VER      : %s\n" "${IFC14XX_TOOLCHAIN_VER}"
  printf "  E3_CC_POKY_TOOLCHAIN_PATH        : %s\n" "${POKY_TOOLCHAIN_PATH}"
  printf "  E3_CC_POKY_TOOLCHAIN_VER         : %s\n" "${POKY_TOOLCHAIN_VER}"
}

options=":t:b:r:c:p:n:m:l:k:hy"
ANSWER="NO"

while getopts "${options}" opt; do
  case "${opt}" in
    t) TARGET=${OPTARG} ;;
    b) BASE_VERSION=${OPTARG} ;;
    r) REQUIRE_VERSION=${OPTARG} ;;
    c) BASE_TAG=${OPTARG} ;;
    n) IFC14XX_TOOLCHAIN_PATH=${OPTARG} ;;
    m) IFC14XX_TOOLCHAIN_VER=${OPTARG} ;;
    l) POKY_TOOLCHAIN_PATH=${OPTARG} ;;
    k) POKY_TOOLCHAIN_VER=${OPTARG} ;;
    y) ANSWER="YES" ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    h)
      usage
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit
      ;;
  esac
done
shift $((OPTIND - 1))

if [ -z "$TARGET" ]; then
  TARGET=${DEFAULT_TARGET_PATH}
fi

if [ -z "$BASE_VERSION" ]; then
  BASE_VERSION=${DEFAULT_BASE_VERSION}
fi

if [ -z "$REQUIRE_VERSION" ]; then
  REQUIRE_VERSION=${DEFAULT_REQ_VERSION}
else
  REQCODE=$(require_code_generator ${REQUIRE_VERSION})
  if [[ ${REQCODE} -le 030003 ]]; then
    printf ">>\n"
    printf "   REQUIRE Version should be larger than or equal to %s\n" "${DEFAULT_REQ_VERSION}"
    REQUIRE_VERSION=${DEFAULT_REQ_VERSION}
  fi
fi

if [ -z "$IFC14XX_TOOLCHAIN_PATH" ]; then
  IFC14XX_TOOLCHAIN_PATH=${DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_PATH}
fi

if [ -z "$IFC14XX_TOOLCHAIN_VER" ]; then
  IFC14XX_TOOLCHAIN_VER=${DEFAULT_E3_CC_IFC14XX_TOOLCHAIN_VER}
fi

if [ -z "$POKY_TOOLCHAIN_PATH" ]; then
  POKY_TOOLCHAIN_PATH=${DEFAULT_E3_CC_POKY_TOOLCHAIN_PATH}
fi

if [ -z "$POKY_TOOLCHAIN_VER" ]; then
  POKY_TOOLCHAIN_VER=${DEFAULT_E3_CC_POKY_TOOLCHAIN_VER}
fi

if [ -z "$BASE_TAG" ]; then
  BASE_TAG=${BASE_VERSION}
fi

EPICS_BASE_TARGET=${TARGET}/base-${BASE_VERSION}
REQUIRE_TARGET=${EPICS_BASE_TARGET}/require/${REQUIRE_VERSION}

case "$1" in
  setup)
    if [ "$ANSWER" == "NO" ]; then
      yes_or_no_to_go
    else
      print_options
    fi
    ;;
  *)
    usage
    ;;
esac

epics_base="${TARGET}/base-${BASE_VERSION}"

config_base="
E3_EPICS_PATH:=${TARGET}
EPICS_BASE_TAG:=tags/R${BASE_TAG}
E3_BASE_VERSION:=${BASE_VERSION}
E3_CC_IFC14XX_TOOLCHAIN_PATH:=${IFC14XX_TOOLCHAIN_PATH}
E3_CC_IFC14XX_TOOLCHAIN_VER:=${IFC14XX_TOOLCHAIN_VER}
E3_CC_POKY_TOOLCHAIN_PATH:=${POKY_TOOLCHAIN_PATH}
E3_CC_POKY_TOOLCHAIN_VER:=${POKY_TOOLCHAIN_VER}
"

local_file=${SC_TOP}/CONFIG_BASE.local
if [[ -f "${local_file}" ]]; then
  rm -f "${local_file}"
fi

cat >"${local_file}" <<EOF
$config_base
EOF

echo ""
echo ">>> EPICS BASE Configuration "
echo ""
echo ">>> CONFIG_BASE.local"
cat "${local_file}"
echo ">>>"

printf "EPICS %s is detected\n" "${BASE_VERSION}"
printf "\n"

config_require="
EPICS_MODULE_TAG:=tags/${REQUIRE_VERSION}
"

local_file=${SC_TOP}/REQUIRE_CONFIG_MODULE.local
if [[ -f "${local_file}" ]]; then
  rm -f "${local_file}"
fi

cat >"${local_file}" <<EOF
$config_require
EOF

echo ">>> REQUIRE_CONFIG_MODULE"
cat "${local_file}"
echo ">>>"

release="
EPICS_BASE:=${epics_base}
E3_REQUIRE_NAME:=require
E3_REQUIRE_VERSION:=${REQUIRE_VERSION}
"

local_file=${SC_TOP}/RELEASE.local
if [[ -f "${local_file}" ]]; then
  rm -f "${local_file}"
fi

cat >"${local_file}" <<EOF
$release
EOF
echo ">>> RELEASE.local"
cat "${local_file}"
echo ">>>"

# We create a config also for development mode, for convenience
local_file=${SC_TOP}/RELEASE_DEV.local
if [[ -f "${local_file}" ]]; then
  rm -f "${local_file}"
fi

cat >"${local_file}" <<EOF
$release
EOF
echo ">>> RELEASE_DEV.local"
cat "${local_file}"
echo ">>>"

echo ""
echo ">>> Run the following..."
echo "    bash e3.bash base"
echo "    bash e3.bash req"
echo "    bash e3.bash -c mod"

exit $?
