#!/usr/bin/env bash
#
#  Copyright (c) 2017 - 2019  Jeong Han Lee
#  Copyright (c) 2017 - 2021  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt

GIT_URL="https://gitlab.esss.lu.se/e3"
GIT_CMD="git clone"

SC_SCRIPT="$(realpath "$0")"
declare -gr SC_SCRIPT
declare -gr SC_SCRIPTNAME="${0##*/}"
declare -gr SC_TOP="${SC_SCRIPT%/*}"
SC_LOGDATE="$(date +%y%m%d%H%M)"
declare -gr SC_LOGDATE

# We use only ONE base, and REQUIRE even if it is used as array
declare -ga base_list=("e3-base")
declare -ga require_list=("e3-require")
declare -ga module_list=()
declare -g require_version=""

# shellcheck source=tools/e3_functions.sh
. "${SC_TOP}"/tools/e3_functions.sh
# shellcheck source=tools/e3_modules_list.sh
. "${SC_TOP}"/tools/e3_modules_list.sh

require_version="$(read_file_get_string "RELEASE.local" "E3_REQUIRE_VERSION:=")"

# BASE
function init_base {
  local rep=""
  for rep in "${base_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      make init || die 1 "${FUNCNAME[*]} : MAKE init ERROR at ${rep}: Please check it"
      if [ "$ANSWER" == "YES" ]; then
        make pkgs
      fi
      make patch || die 1 "${FUNCNAME[*]} : MAKE patch ERROR at ${rep}: Please check it"
      make vars
      popd || die
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist. Please make sure \"make gbase\" first"
    fi
    printf "\n"
  done
}

# BASE
function setup_base {
  clone_base
  init_base
}

## REQUIRE
function setup_require {
  clone_require
  init_targets "" "${require_list[@]}"
}

function init_targets {
  local prefix="$1"
  shift
  local targets=("$@")
  local rep=""
  for rep in "${targets[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      make "$prefix"init || die 1 "${FUNCNAME[*]} : MAKE init ERROR at ${rep}: Please check it"
      make "$prefix"patch
      make "$prefix"vars
      popd || die
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist. Please make sure to clone it first"
    fi
    printf "\n"
  done
}

function setup_modules {
  clone_modules
  init_targets "" "${module_list[@]}"
}

function build_targets {
  local prefix="$1"
  shift
  local targets=("$@")
  local rep
  for rep in "${targets[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      # todo: parallel cannot be quoted because it may be an empty string
      # shellcheck disable=SC2086
      make -s ${PARALLEL} "$prefix"build || die 1 "${FUNCNAME[*]} : Building Error at ${rep}: Please check the building error"
      make -s "$prefix"install || die 1 "${FUNCNAME[*]} : MAKE INSTALL ERROR at ${rep}: Please check it"
      popd || die
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist"
    fi
    printf "\n"
  done
  generate_setenv
}

function get_setE3Env_path {
  if [ ! -d "${SC_TOP}"/e3-require ]; then
    echo "e3-require directory does not exist in ${SC_TOP}" >&2
    return
  fi
  local modules_path
  modules_path=$(make -C "${SC_TOP}"/e3-require/ vars | grep ^E3_MODULES_PATH | cut -f 3 -d ' ')
  local module_name
  module_name=$(make -C "${SC_TOP}"/e3-require/ vars | grep ^E3_MODULE_NAME | cut -f 3 -d ' ')
  local module_version
  module_version=$(make -C "${SC_TOP}"/e3-require/ vars | grep ^E3_MODULE_VERSION | cut -f 3 -d ' ')
  echo "${modules_path}/${module_name}/${module_version}/bin/setE3Env.bash"
}

function module_loading_test_on_iocsh {
  pushd "${SC_TOP}"/e3-require || die
  make requireconf
  # shellcheck source=/dev/null
  source "$(get_setE3Env_path)"
  popd || die

  local IOC_TEST
  IOC_TEST=$(mktemp)

  {
    local PREFIX_MODULE="EPICS_MODULE_NAME:="
    local PREFIX_LIBVERSION="E3_MODULE_VERSION:="
    local mod=""
    local ver=""
    printf "var requireDebug 1\n"
    for rep in "${module_list[@]}"; do
      while read -r line; do
        if [[ $line =~ ${PREFIX_LIBVERSION} ]]; then
          ver=${line#$PREFIX_LIBVERSION}
        elif [[ $line =~ ${PREFIX_MODULE} ]]; then
          mod=${line#$PREFIX_MODULE}
        fi
      done <"${SC_TOP}"/"${rep}"/configure/CONFIG_MODULE
      if [ "${mod}" = "StreamDevice" ]; then
        mod="stream"
      fi
      printf "#\n#\n"
      printf "# >>>>> MODULE Loading ........\n"
      printf "# >>>>> MODULE NAME ..... %s\n" "${mod}"
      printf "# >>>>>        VER  ..... %s\n" "${ver}"
      printf "require %s,%s\n" "${mod}" "${ver}"
      printf "# >>>>>\n"
      printf "#\n#\n"
    done

  } >"${IOC_TEST}"

  exec iocsh.bash "${IOC_TEST}"
}

function generate_setenv {
  local E3ENVFILE=""
  local SETENV="${SC_TOP}/tools/setenv"
  local E3ENVFILE
  E3ENVFILE=$(get_setE3Env_path)

  printf ">>\n"
  if [[ -f "${SETENV}" ]]; then
    printf "  We've found the existent %s\n" "${SETENV}"
    printf "  Rename it to %s\n" "${SETENV}"_"${SC_LOGDATE}"
    printf "  Still, it can be used in order to set old existent e3.\n"
    mv "${SETENV}" "${SETENV}"_"${SC_LOGDATE}"
  fi
  printf ">>\n"
  printf "  Creating %s .... \n" "${SETENV}"
  printf "  Please, source it, if one would like to activate latest installed e3.\n"
  printf "  source tools/setenv \n"
  echo "source ${E3ENVFILE}" >"${SETENV}"
}

function all_base {
  clean_base
  setup_base
  build_targets "" "${base_list[@]}"
}

function all_require {
  local prefix="$1"
  clean_require
  setup_require
  build_targets "$prefix" "${require_list[@]}"
}

function all_modules {
  clean_modules
  setup_modules
  build_targets "" "${module_list[@]}"
}

function configuration_update_modules {
  local rep=""
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      printf "We are updating the configuration for %s\n", "${rep}"
      # todo: this function will need to be revised some time in the future
      e3TemplateGenerator -u "${rep}" || die "you need e3TemplateGenerator on PATH for this"
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist"
    fi
    printf "\n"
  done
}

function git_diff_modules {
  local rep=""
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      printf "We are updating the configuration for %s\n", "${rep}"
      pushd "${rep}" || die
      git diff
      popd || die
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist"
    fi
    printf "\n"
  done
}

function show_tag {
  local rep=""
  local prefix="*e3-"
  for rep in "${module_list[@]}"; do
    if [[ -d "${rep}" ]]; then
      pushd "${rep}" || die
      atag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
      printf "<project path=\"0.00.000.%s\" name=\"%s\" revision=\"refs/tags/%s\" groups=\"\" />" "${rep#"$prefix"}" "$rep" "$atag"
      popd || die
    else
      die 1 "${FUNCNAME[*]} : ${rep} doesn't exist"
    fi
    printf "\n"
  done
}

function usage {
  usage_title
  usage_mod

  {
    echo " < option > "
    echo ""
    echo "           env    : Print enabled Modules"
    echo ""
    echo "           call   : Clean all (base, require, selected module group)"
    echo "           gall   : Clone all (base, require, selected module group)"
    echo "            all   : call, gall, ibase, bbase, ireq, breq, imod, bmod"
    echo ""
    echo "           cbase  : Clean Base"
    echo "           gbase  : Clone Base"
    echo "           ibase  : Init  Base "
    echo "           bbase  : Build, Install Base"
    echo "            base  : cbase, gbase, ibase, bbase"
    echo ""
    echo "           creq   : Clean Require"
    echo "           greq   : Clone Require"
    echo "           ireq   : Init  Require"
    echo "           breq   : Build, Install Require"
    echo "            req   : creq, gbase ireq, breq"
    echo ""
    echo "           cmod   : Clean Modules (selected module group)"
    echo "           gmod   : Clone Modules (selected module group)"
    echo "           imod   : Init  Modules (selected module group)"
    echo "           bmod   : Build, Install Modules (selected module group)"
    echo "            mod   : cmod, mod, imod, bmod"
    echo ""
    echo "        co_base \"_check_out_name_\" : Checkout Base"
    echo "         co_req \"_check_out_name_\" : Checkout Require"
    echo "         co_mod \"_check_out_name_\" : Checkout Modules  (selected module group)"
    echo "         co_all \"_check_out_name_\" : co_base, co_req, co_mod"
    echo ""
    echo "          vbase   : Print BASE    Version Information in e3-*"
    echo "           vreq   : Print REQUIRE Version Information in e3-*"
    echo "           vmod   : Print MODULES Version Information in e3-*"
    echo "           vall   : Print ALL     Version Information in e3-*"
    echo "           vins   : Print INSTALLED Version Information for locally installed e3 modules"
    echo ""
    echo "         allall   : Print ALL Version Information in e3-* by using \"make vars\""
    echo ""
    echo "           load   : Load all installed Modules into iocsh.bash"
    echo "        setenv    : create setenv in ${SC_TOP}/tools"
    echo ""
    echo "           vers   : Print Source Tag / Module Versions"
    echo "            dep   : Print DEP_VERSION information"
    echo "        plotdep   : Plot the naive module depdendency drawing"
    echo "      closeplot   : Close all active opened plots"
    echo ""
    echo "           tags   : Print the latest tag for a repo manifest"
    echo ""
    echo ""
    echo ""
    echo "  Examples : "
    echo ""
    echo "          $0 creq "
    echo "          $0 -ctpifealb cmod"
    echo "          $0 -t env"
    echo "          $0 base"
    echo "          $0 req"
    echo "          $0 -e cmod"
    echo "          $0 -t load"
    echo "   "
    echo ""
  } 1>&2
  exit 1
}

while getopts "${options}" opt; do
  case "${opt}" in
    c) common="1" ;;
    t) ts="1" ;;
    p) psi="1" ;;
    i) ifc="1" ;;
    e) ecat="1" ;;
    a) area="1" ;;
    s) ps="1" ;;
    v) vac="1" ;;
    l) rf="1" ;;
    b) bi="1" ;;
    o) only="1" ;;
    *) usage ;;
  esac
done
shift $((OPTIND - 1)) # remove parsed options and args from $@ list

module_list_func

# print_list
case "$1" in
  *mod)
    echo ">> Selected Modules are :"
    echo "${module_list[@]}"
    echo ""
    ;;
  *all)
    echo ">> Selected Modules are :"
    echo "${module_list[@]}"
    echo ""
    ;;
  *) ;;

esac

filter="pkg"

arg=$2

if test "${arg#*$filter}" != "$arg"; then
  ANSWER="YES"
fi

filter2="para"
if test "${arg#*$filter2}" != "$arg"; then
  PARALLEL="-j -l 4"
fi

case "$1" in
  env) print_module_list ;;
  vars) print_module_list ;;
  # all : clean, clone, init, build, and all
  clean) clean_all ;;
  call) clean_all ;;
  gall) clone_all ;;
  # BASE : clean, clone, init, build, and all
  cbase) clean_base ;;
  gbase) clone_base ;;
  ibase) init_base ;;
  bbase) build_targets "" "${base_list[@]}" ;;
  base) all_base ;;
  # REQUIRE : clean, clone, init, build, and all
  creq) clean_require ;;
  greq) clone_require ;;
  ireq) init_targets "" "${require_list[@]}" ;;
  devireq) init_targets "dev" "${require_list[@]}" ;;
  breq) build_targets "" "${require_list[@]}" ;;
  req) all_require ;;
  devreq) all_require "dev" ;;
  # MODULES : clean, clone, init, build, and all
  cmod) clean_modules ;;
  gmod) clone_modules ;;
  imod) init_targets "" "${module_list[@]}" ;;
  bmod) build_targets "" "${module_list[@]}" ;;
  mod) all_modules ;;
  pmod) git_pull_modules ;;
  # Git Checkout
  co_base) git_checkout_base "$2" ;;
  co_req) git_checkout_require "$2" ;;
  co_mod) git_checkout_modules "$2" ;;
  co_all) git_checkout_all "$2" ;;
  # GIT Commands
  pull) git_pull_all ;;
  add) git_add "$2" ;;
  commit) git_commit "$2" ;;
  push) git_push ;;
  # Module Loading Test
  load) module_loading_test_on_iocsh ;;
  setenv) generate_setenv ;;
  # Print Version Information in e3-* directory
  vbase) print_version_info_base ;;
  vreq) print_version_info_require ;;
  vmod) print_version_info_modules ;;
  vall) print_version_info_all ;;
  vins) print_installed_modules "" "${module_list[@]}" ;;
  # Call *make vars in each e3-* directory
  allall) print_version_really_everything ;;
  cupdate) configuration_update_modules ;;
  gitdiff) git_diff_modules ;;
  # show version and depdendcy
  vers) exec_makefile_rule "vers" ;;
  dep) exec_makefile_rule "dep" ;;
  plotdep) exec_makefile_rule "plotdep" ;;
  closeplot) exec_makefile_rule "closeplot" ;;
  # show the latest tag
  tags) show_tag ;;
  *) usage ;;
esac

exit 0
