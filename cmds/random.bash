#!/usr/bin/env bash

SC_SCRIPT="$(realpath "$0")"
declare -gr SC_SCRIPT
SC_TOP="${SC_SCRIPT%/*}"
declare -gr SC_TOP

C="$(tr -cd 0-9 </dev/urandom | head -c 8)"
sed -e "s:_random_:$C:g" <"${SC_TOP}"/random.in >"${SC_TOP}"/random.cmd
